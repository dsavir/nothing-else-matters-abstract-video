# Overview
Program to get abstract video from mp3 file. Based on the following:
1. https://funprogramming.org/126-A-Processing-abstract-video-player.html
2. http://code.compartmental.net/minim/beatdetect_class_beatdetect.html
3. https://funprogramming.org/VideoExport-for-Processing withAudioViz example
4. Example mp3 in repository is Chopin's Nocturne in E-flat, downloaded from https://www.mfiles.co.uk/mp3-files.htm

# Example
Check out the video for [my cover](https://www.youtube.com/watch?v=A_dMCVJ7tUE) of Nothing Else Matters, by Metallica.

# Instructions  
1. Set firstPass = true. 
2. Copy your mp3 file into data folder and change name below to your filename (without "data/", leave as is).
3. Set window size to desired size, and fps to desired fps (default is 30 and fullscreen)
4. Run the file. It will play the song but show a black screen. It will exit when song is done.
5. Change firstPass to false;
6. Run the file again. It will show the abstract video without playing the song. It will exit when done.
7. When done, your movie will be saved to processing-movie.mp4.

## WARNING: 
if you run this again, previous processing-movie.mp4 will be erased!

# Explanation:
This program runs once on the song and saves relevant data to a csv file. The second time, it loads the data from the csv file, draws the video
according to saved data, and saves each frame using Video Export. You can try to do this in one pass, but then the screen needs to be relatively small and no more than 20 fps 
(at least on my computer) because otherwise the audio and video become out of sync.

# License

**Copyright 2019 Danielle Honigstein**
https://gitlab.com/dsavir/nothing-else-matters-abstract-video

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
USE OR OTHER DEALINGS IN THE SOFTWARE.
