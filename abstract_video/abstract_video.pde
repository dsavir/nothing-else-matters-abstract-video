/**
Program to get abstract video from mp3 file. Based on the following:
1. https://funprogramming.org/126-A-Processing-abstract-video-player.html
2. http://code.compartmental.net/minim/beatdetect_class_beatdetect.html
3. https://funprogramming.org/VideoExport-for-Processing withAudioViz example
4. Example mp3 is Chopin's nocturne in E-flat from https://www.mfiles.co.uk/mp3-files.htm

Instructions: 
1. Set firstPass = true. 
2. Copy your mp3 file into data folder and change name below to your filename (without "data/", as is).
3. Set window size to desired size, and fps to desired fps (default is 30 and fullscreen)
4. Run the file. It will play the song but show a black screen. It will exit when song is done.
5. Change firstPass to false;
6. Run the file again. It will show the abstract video without playing the song. It will exit when done.
7. When done, your movie will be saved to processing-movie.mp4.

WARNING: if you run this again, previous processing-movie.mp4 will be erased!

Explanation:
This program runs once on the song and saves relevant data to a csv file. The second time, it loads the data from the csv file, draws the
according to saved data, and saves each frame using Video Export. You can try to do this in one pass, but hten the screen needs to be relativly small and no more than 20 fps 
(at least on my computer) because otherwise the audio and video become out of sync.

Copyright 2019 Danielle Honigstein

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), 
to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, 
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF 
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE 
USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

//video exporter. get from tools->add tools->libraries->video exporter
import com.hamoid.*;
//minim. get from tools->add tools->libraries->minim
import ddf.minim.*;
import ddf.minim.analysis.*;

//Minim stuff - to play and analyze the song
Minim minim;
AudioPlayer song;
FFT fft;
BeatDetect beat;
float eRadius; //radius of beat detect circle
int counter = 0; //fft counter to get h,s,b
float h, s, b, a; //h s b are HSB color values. We get htis from the FFT. a is the alpha of the beat detect, we get this from beat detect.

//video export stuff
VideoExport videoExport;
String audioFilePath = "chopin-nocturne-op9-no2.mp3";
String textFilePath = audioFilePath + ".txt";
float movieFPS = 30;

//file i/o
BufferedReader reader;
PrintWriter output;

//Is this the first pass or second?
boolean firstPass = true;
//firstpass: get hsb,a and  eRadius data, and save to file. you will hear hte msic and see a black
//secondPass: read data that we saved and draw and export the video. You will see the video and not hear music

//setup what we need
void setup()
{
  //size(1280, 720);//set the size you wish
  fullScreen();
  frameRate(movieFPS);
  background(0);
  
  if (firstPass) {// create minim to play song and create fft and beatdetect for analysis
    println("firstPass");//debug
    // always start Minim first!
    minim = new Minim(this);
    output = createWriter(dataPath(textFilePath));//filewriter

    // specify 512 for the length of the sample buffers
    // the default buffer size is 1024
    song = minim.loadFile(audioFilePath, 512);
    song.play();

    // an FFT needs to know how 
    // long the audio buffers it will be analyzing are
    // and also needs to know 
    // the sample rate of the audio it is analyzing
    fft = new FFT(song.bufferSize(), song.sampleRate());

    beat = new BeatDetect();//beat detection analysis
    
    eRadius = 20;//initialize eRadius

    println("firstpass finished setup");//debug
  } else { //second pass - prepare to read data and setup videoExport
    println("secondpass!");//debug
    //read file we just created
    reader = createReader(textFilePath);
    // Set up the video exporting
    videoExport = new VideoExport(this);
    videoExport.setFrameRate(movieFPS);
    videoExport.setAudioFileName(audioFilePath);
    videoExport.startMovie();
    //set drawing mode
    ellipseMode(RADIUS);
    println("secondpass finished setup");//debug
  }
}

//this is where the action takes place
void draw()
{
  if (firstPass) {//calcuatae and save
    println("drawing " + frameCount);//debug
    // first perform a forward fft on one of song's buffers
    // I'm using the mix buffer
    //  but you can use any one you like
    fft.forward(song.mix);
    //get count of bands and max value of fft band
    int count = 0;// the number of bands above threshold
    float max = 0;//max value of band
    float thresh = 0.5;//threshold to count bands
    for (int i = 0; i < fft.specSize(); i++)
    {
      float fftBand = fft.getBand(i);
      if (fftBand>max) {
        max = fftBand;
      }
      if (fftBand>thresh) {
        count = count + 1;
      }
    }
    //define h,s,b
    h = (count*3.5)%360;
    s = max*5;
    b = abs(song.left.get(0)*5000);//100
    //detect beat and calcuate a, eRadius
    beat.detect(song.mix);
    a = map(eRadius, 20, 80, 60, 255);
    if ( beat.isOnset() ) eRadius = 80;
    eRadius *= 0.95;
    if ( eRadius < 20 ) eRadius = 20;
    //save to file
    output.println(h + "," + s + "," + b + "," + a + "," + eRadius);
    
    if (!(song.isPlaying())) {//song has finished - close and exit.
      println("finished song");//debug
      output.flush();
      output.close();
      exit();
    }
  } else {//show video and save
    println("drawing " + frameCount);//debug
    //read line from file
    String line;
    try {
      line = reader.readLine();
    }
    catch (IOException e) {
      e.printStackTrace();
      line = null;
    }
    if (line == null) {
      // Done reading the file.
      // Close the video file.
      println("finished file");
      videoExport.endMovie();
      exit();
    } else {
      //get data from line
      String[] p = split(line, ",");
      h = float(p[0]);
      s = float(p[1]);
      b = float(p[2]);
      a = float(p[3]);
      eRadius = float(p[4]);
      //draw hsb circles
      int x = width/2;
      int y = height/2;
      fill(h, s, 100);
      //s = min(s,100);
      colorMode(HSB, 360);
      for (int i=0; i<10; i++) {
        ellipse(x, y, s/2, s/2);
        x += sin(b/30) * s*4.5;
        y += cos(b/30) * s *4.5;
        if (x<0 || y<0 || x >= width || y>= height) {
          break;
        }
      }
      //draw beat
      colorMode(RGB);
      fill(30, 60, 250, a);
      ellipse(width/2, height/2, eRadius, eRadius);
      //save frame
      videoExport.saveFrame();
    }
  }
}
